# VLC

VLC application in docker

## Build

```bash
docker build -t vlc .
```

## Execute

```bash
docker run --rm \
    --detach \
    --privileged \
    --name vlc \
    --device /dev/snd \
    --device /dev/dri \
    --group-add audio \
    --env="DISPLAY :0" \
    --ipc="host" \
    -e DISPLAY=unix$DISPLAY \
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    -v /etc/localtime:/etc/localtime:ro \
    -v $PWD:/home/vlc/media \
    -v $VLC_HOST_CONFIG_DIR:/home/vlc/.config \
    -v $VLC_HOST_CACHE_DIR:/home/vlc/.cache \
    -v $VLC_HOST_SNAPSHOTS_DIR:/home/vlc/snapshots \
    vlc
```