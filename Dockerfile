FROM debian

ARG VLC_UID="1000"
ARG VLC_GID="1000"

ENV DEBIAN_FRONTEND noninteractive

RUN groupadd -g "$VLC_GID" vlc && \
    useradd -m -s /bin/sh -u "$VLC_UID" -g "$VLC_GID" vlc && \
    apt-get update  -y &&  \
    apt-get upgrade -y
    
RUN apt-get install xauth -y
RUN apt-get install \
        --no-install-recommends \
		ca-certificates \
		libgl1-mesa-dri \
		libgl1-mesa-glx \
		pulseaudio \
		alsa-utils \
		dbus* \
        vlc -y

RUN rm -rf /var/lib/apt/lists/*

RUN touch ~/.Xauthority

USER "vlc"

WORKDIR "/home/vlc/media"
VOLUME ["/home/vlc/media"]

RUN vlc --reset-config --reset-plugins-cache

ENTRYPOINT ["/usr/bin/vlc", "--no-qt-privacy-ask", "--no-metadata-network-access", "--snapshot-path=/home/vlc/snapshots"]